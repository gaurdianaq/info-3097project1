package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.ActionTimedMessage
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R
import com.gaurdianaq.hauntedai.TimedMessage
import com.gaurdianaq.hauntedai.startTimedMessage

class Response : QuizFragment()
{
    companion object {
        fun newInstance() = Response()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.response_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val timedMessage:TimedMessage
        if (viewModel.responseCorrect)
        {
            timedMessage = TimedMessage(getString(R.string.responseCorrect),2)
        }
        else
        {
            timedMessage = TimedMessage(getString(R.string.responseIncorrect),2)
        }

        startTimedMessage(message, arrayOf(timedMessage, ActionTimedMessage("", 0, loadNextScreen)))
    }
}
package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.ActionTimedMessage
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R
import com.gaurdianaq.hauntedai.TimedMessage
import com.gaurdianaq.hauntedai.startTimedMessage

class Finish : QuizFragment()
{
    companion object {
        fun newInstance() = Finish()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.finish_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.finish1), 3),
            TimedMessage(getString(R.string.finish2), 3),
            TimedMessage(getString(R.string.finish3), 3),
            TimedMessage(getString(R.string.finish4), 2),
            TimedMessage(getString(R.string.finish5), 3),
            TimedMessage(getString(R.string.finish6), 3),
            ActionTimedMessage(getString(R.string.finish6), 0, finish)
        )

        startTimedMessage(message, messages)
    }

    val finish: ()->Unit = fun()
    {
        activity?.finish()
    }
}
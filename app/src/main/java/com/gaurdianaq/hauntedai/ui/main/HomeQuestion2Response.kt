package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class HomeQuestion2Response : QuizFragment() {


    companion object {
        fun newInstance() = HomeQuestion2Response()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homequestion2response_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val messages:Array<TimedMessage>
        viewModel.nextScreen = HomeQuestion3.newInstance()
        if (viewModel.lookedAround && !viewModel.lied)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion2Response), 3),
                                TimedMessage(getString(R.string.homeQuestion2LookedAway1), 3),
                                TimedMessage(getString(R.string.homeQuestion2LookedAway2), 3),
                                TimedMessage(getString(R.string.homeQuestion2Truth), 3),
                                ActionTimedMessage(getString(R.string.homeQuestion2Truth), 0, loadNextScreen)
            )
        }
        else if (viewModel.lookedAround && viewModel.lied)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion2Response), 3),
                TimedMessage(getString(R.string.homeQuestion2LookedAway1), 3),
                TimedMessage(getString(R.string.homeQuestion2LookedAway2), 3),
                TimedMessage(getString(R.string.homeQuestion2Lie1), 2),
                TimedMessage(getString(R.string.homeQuestion2Lie2), 3),
                ActionTimedMessage(getString(R.string.homeQuestion2Lie2), 0, loadNextScreen)
            )
        }
        else if (!viewModel.lookedAround && !viewModel.lied)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion2Response), 3),
                TimedMessage(getString(R.string.homeQuestion2Stare1), 3),
                TimedMessage(getString(R.string.homeQuestion2Stare2), 3),
                TimedMessage(getString(R.string.homeQuestion2Truth), 3),
                ActionTimedMessage(getString(R.string.homeQuestion2Truth), 0, loadNextScreen)
            )
        }
        else
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion2Response), 3),
                TimedMessage(getString(R.string.homeQuestion2Stare1), 3),
                TimedMessage(getString(R.string.homeQuestion2Stare2), 3),
                TimedMessage(getString(R.string.homeQuestion2Lie1), 2),
                TimedMessage(getString(R.string.homeQuestion2Lie2), 3),
                ActionTimedMessage(getString(R.string.homeQuestion2Lie2), 0, loadNextScreen)
            )
        }

        startTimedMessage(message, messages)
    }

}

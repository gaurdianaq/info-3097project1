package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R

class HomeQuestion3 : QuizFragment()
{
    private lateinit var choice1: QuizActionButton
    private lateinit var choice2: QuizActionButton

    companion object {
        fun newInstance() = HomeQuestion3()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homequestion3_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        choice1 = activity!!.findViewById(R.id.choice1)
        choice1.setUpButton(yes, HomeQuestion4.newInstance())
        choice2 = activity!!.findViewById(R.id.choice2)
        choice2.setUpButton(no, HomeQuestion4.newInstance())
    }

    val yes: ()->Unit = fun()
    {
        viewModel.alone = true
    }

    val no: ()->Unit = fun()
    {
        viewModel.alone = false
    }
}
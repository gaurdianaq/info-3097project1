package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class HomeQuestion4Response : QuizFragment() {


    companion object {
        fun newInstance() = HomeQuestion4Response()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homequestion4response_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val messages:Array<TimedMessage>
        viewModel.nextScreen = HomeQuestion5.newInstance()
        if (viewModel.alonecertain && viewModel.lookedAround)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion4YesLooked), 3),
                ActionTimedMessage(getString(R.string.homeQuestion4YesLooked), 0, loadNextScreen))
        }
        else if (viewModel.alonecertain && !viewModel.lookedAround)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion4YesNoLook), 3),
                ActionTimedMessage(getString(R.string.homeQuestion4YesNoLook), 0, loadNextScreen))
        }
        else if (!viewModel.alonecertain && viewModel.alone && viewModel.lookedAround)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion4NoAloneLooked1), 3),
                                TimedMessage(getString(R.string.homeQuestion4NoAloneLooked2), 3),
                                ActionTimedMessage(getString(R.string.homeQuestion4NoAloneLooked2), 3, loadNextScreen)
            )
        }
        else if (!viewModel.alonecertain && !viewModel.alone && viewModel.lookedAround)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion4NoNotAloneLooked1), 3),
                                TimedMessage(getString(R.string.homeQuestion4NoNotAloneLooked2), 3),
                                ActionTimedMessage(getString(R.string.homeQuestion4NoNotAloneLooked2), 3, loadNextScreen)
            )
        }
        else if (!viewModel.alonecertain && !viewModel.alone && !viewModel.lookedAround)
        {
            messages = arrayOf(TimedMessage(getString(R.string.homeQuestion4NoNotAloneNoLook1), 3),
                                TimedMessage(getString(R.string.homeQuestion4NoNotAloneNoLook2), 3),
                                ActionTimedMessage(getString(R.string.homeQuestion4NoNotAloneNoLook2), 3, loadNextScreen)
            )
        }
        else
        {
            messages = arrayOf(
                TimedMessage(getString(R.string.homeQuestion4NoAloneNoLook), 3),
                ActionTimedMessage(getString(R.string.homeQuestion4NoAloneNoLook), 3, loadNextScreen))
        }

        startTimedMessage(message, messages)
    }

}

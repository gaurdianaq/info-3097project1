package com.gaurdianaq.hauntedai.ui.main

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import kotlinx.android.synthetic.main.resiliencytest_fragment.*

class ResiliencyFailure : QuizFragment()
{
    companion object {
        fun newInstance() = ResiliencyFailure()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.resiliencyfailure_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.nextScreen = Finish.newInstance()
        val mainActivity = activity as MainActivity
        mainActivity.mediaPlayer?.stop()
        mainActivity.mediaPlayer = MediaPlayer.create(activity, R.raw.openyoureyes)
        mainActivity.mediaPlayer?.setOnCompletionListener(fun(mp:MediaPlayer){
            mainActivity.mediaPlayer = MediaPlayer.create(activity, R.raw.ambiance1)
            mainActivity.mediaPlayer?.isLooping = true
            mainActivity.mediaPlayer?.start()
        })
        mainActivity.mediaPlayer?.start()

        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.resiliencyFailure1), 3),
            TimedMessage(getString(R.string.resiliencyFailure2),3),
            TimedMessage(getString(R.string.resiliencyFailure3), 2),
            ActionTimedMessage(getString(R.string.resiliencyFailure3), 0, loadNextScreen)
        )

        startTimedMessage(message, messages)
    }
}
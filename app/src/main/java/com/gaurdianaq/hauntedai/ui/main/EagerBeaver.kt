package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class EagerBeaver : QuizFragment() {

    companion object {
        fun newInstance() = EagerBeaver()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.eagerbeaver_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.eagerBeaver1), 5),
            TimedMessage(getString(R.string.eagerBeaver2), 5),
            TimedMessage(getString(R.string.eagerBeaver3), 5),
            TimedMessage(getString(R.string.eagerBeaver4), 5),
            TimedMessage(getString(R.string.eagerBeaver5), 5),
            TimedMessage(getString(R.string.eagerBeaver6), 5),
            TimedMessage(getString(R.string.eagerBeaver7), 1),
            TimedMessage(getString(R.string.eagerBeaver8), 1),
            TimedMessage(getString(R.string.eagerBeaver9), 1),
            TimedMessage(getString(R.string.eagerBeaver10), 1),
            TimedMessage(getString(R.string.eagerBeaver11), 1),
            TimedMessage(getString(R.string.eagerBeaver12), 1),
            TimedMessage(getString(R.string.eagerBeaver13), 1),
            TimedMessage(getString(R.string.eagerBeaver14), 1),
            TimedMessage(getString(R.string.eagerBeaver15), 1),
            TimedMessage(getString(R.string.eagerBeaver16), 1),
            TimedMessage(getString(R.string.eagerBeaver17), 1),
            TimedMessage(getString(R.string.eagerBeaver18), 1),
            TimedMessage(getString(R.string.eagerBeaver19), 1),
            TimedMessage(getString(R.string.eagerBeaver20), 1),
            ActionTimedMessage(getString(R.string.eagerBeaver21), 1, loadNextScreen)

        )
        setOnLookAwayListener(object : Listener {
            override fun doTask() {
                viewModel.lookedAround = true
            }
        })
        viewModel.lookedAround = false
        viewModel.nextScreen = HomeQuestion1.newInstance()
        startTimedMessage(message, messages)
    }

}

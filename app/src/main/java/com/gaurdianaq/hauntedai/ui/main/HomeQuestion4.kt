package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R

class HomeQuestion4 : QuizFragment()
{
    private lateinit var choice1: QuizActionButton
    private lateinit var choice2: QuizActionButton

    companion object {
        fun newInstance() = HomeQuestion4()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homequestion4_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        choice1 = activity!!.findViewById(R.id.choice1)
        choice1.setUpButton(yes, HomeQuestion4Response.newInstance())
        choice2 = activity!!.findViewById(R.id.choice2)
        choice2.setUpButton(no, HomeQuestion4Response.newInstance())
    }

    val yes: ()->Unit = fun()
    {
        viewModel.alonecertain = true
    }

    val no: ()->Unit = fun()
    {
        viewModel.alonecertain = false
    }
}
package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class HomeSegway : QuizFragment() {

    private lateinit var submitBtn:QuizActionButton

    companion object {
        fun newInstance() = HomeSegway()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homesegway_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        submitBtn = activity!!.findViewById(R.id.submitBtn)
        submitBtn.setUpButton(EagerBeaver.newInstance())
        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.homeSegwayPart1), 2),
            TimedMessage(getString(R.string.homeSegwayPart2), 2),
            ActionTimedMessage(getString(R.string.homeSegwayPart3), 3, notAnEagerBeaver),
            TimedMessage(getString(R.string.homeSegwayPart4), 4)
        )
        setOnLookAwayListener(object : Listener {
            override fun doTask() {
                viewModel.lookedAround = true
            }
        })
        startTimedMessage(message, messages)
    }

    val notAnEagerBeaver: ()->Unit = fun()
    {
        submitBtn.setUpButton(HomeQuestion1.newInstance())
    }

}

package com.gaurdianaq.hauntedai.ui.main

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.MainActivity
import com.gaurdianaq.hauntedai.R


class ResiliencyTest : QuizFragment()
{
    companion object {
        fun newInstance() = ResiliencyTest()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.resiliencytest_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.nextScreen = ResiliencySuccess.newInstance()
        val mainActivity = activity as MainActivity
        mainActivity.mediaPlayer?.stop()
        mainActivity.mediaPlayer = MediaPlayer.create(activity, R.raw.homeinvasion)
        mainActivity.mediaPlayer?.start()
        mainActivity.mediaPlayer?.setOnCompletionListener(fun (mp:MediaPlayer) {
            loadNextScreen.invoke()
        })

        setOnLeftOpenListener(object : Listener {
            override fun doTask() {
                viewModel.nextScreen = ResiliencyFailure.newInstance()
                loadNextScreen.invoke()
            }
        })

        setOnRightOpenListener(object : Listener {
            override fun doTask() {
                viewModel.nextScreen = ResiliencyFailure.newInstance()
                loadNextScreen.invoke()
            }
        })

        setOnLookAwayListener(object : Listener {
            override fun doTask() {
                viewModel.nextScreen = ResiliencyFailure.newInstance()
                loadNextScreen.invoke()
            }
        })

    }
}
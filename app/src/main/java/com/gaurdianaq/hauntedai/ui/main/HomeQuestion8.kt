package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R

class HomeQuestion8 : QuizFragment()
{
    private lateinit var choice1: QuizActionButton
    private lateinit var choice2: QuizActionButton
    private lateinit var choice3: QuizActionButton
    private lateinit var choice4: QuizActionButton
    private lateinit var choice5: QuizActionButton

    companion object {
        fun newInstance() = HomeQuestion8()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.homequestion8_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        choice1 = activity!!.findViewById(R.id.choice1)
        choice1.setUpButton(ResilienceSegway.newInstance())
        choice2 = activity!!.findViewById(R.id.choice2)
        choice2.setUpButton(ResilienceSegway.newInstance())
        choice3 = activity!!.findViewById(R.id.choice3)
        choice3.setUpButton(ResilienceSegway.newInstance())
        choice4 = activity!!.findViewById(R.id.choice4)
        choice4.setUpButton(ResilienceSegway.newInstance())
        choice5 = activity!!.findViewById(R.id.choice5)
        choice5.setUpButton(ResilienceSegway.newInstance())
    }
}
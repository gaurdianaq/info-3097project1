package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class ResilienceSegway : QuizFragment() {

    companion object {
        fun newInstance() = ResilienceSegway()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.resiliencesegway_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.nextScreen = ResiliencyTest.newInstance()
        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.resiliencySegwayPart1), 2),
            TimedMessage(getString(R.string.resiliencySegwayPart2), 2),
            TimedMessage(getString(R.string.resiliencySegwayPart3), 3),
            TimedMessage(getString(R.string.resiliencySegwayPart4), 4),
            TimedMessage(getString(R.string.resiliencySegwayPart5), 3),
            ActionTimedMessage(getString(R.string.resiliencySegwayPart6), 0, setListeners)
        )

        startTimedMessage(message, messages)
    }

    val setListeners: ()->Unit = fun()
    {
        setOnLeftClosedListener(object : Listener {
            override fun doTask() {
                loadNextScreen.invoke()
            }
        })

        setOnRightClosedListener(object : Listener {
            override fun doTask() {
                loadNextScreen.invoke()
            }
        })
    }

}

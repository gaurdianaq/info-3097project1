package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton

import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R
import java.lang.NumberFormatException


class LogicQuestion1 : QuizFragment() {

    private lateinit var textFieldAnswer:EditText
    private lateinit var btnSubmit:QuizActionButton

    companion object {
        fun newInstance() = LogicQuestion1()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.logicquestion1_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        textFieldAnswer = activity!!.findViewById(R.id.answerBox)
        btnSubmit = activity!!.findViewById(R.id.submitBtn)
        btnSubmit.setUpButton(checkResponse, Response.newInstance())
        viewModel.nextScreen = LogicQuestion2.newInstance()
    }

    private val checkResponse: () -> Unit = fun()
    {
        try {
            viewModel.responseCorrect = textFieldAnswer.text.toString().toFloat() == -2.0f
        }
        catch (ex:NumberFormatException) {
            viewModel.responseCorrect = false
        }

    }
}

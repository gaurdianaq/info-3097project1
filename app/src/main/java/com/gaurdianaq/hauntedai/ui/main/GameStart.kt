package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment


class GameStart : QuizFragment() {
    companion object {
        fun newInstance() = GameStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.gamestart_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.nextScreen = LogicQuestion1.newInstance()
        val messages:Array<TimedMessage> = arrayOf(
            TimedMessage(getString(R.string.gameStartMsg1), 2),
            TimedMessage(getString(R.string.gameStartMsg2), 2),
            TimedMessage(getString(R.string.gameStartMsg3), 3),
            TimedMessage(getString(R.string.gameStartMsg4), 4),
            TimedMessage(getString(R.string.gameStartMsg5), 3),
            ActionTimedMessage(getString(R.string.gameStartMsg5), 0, loadNextScreen)
        )

        startTimedMessage(message, messages)
    }

}

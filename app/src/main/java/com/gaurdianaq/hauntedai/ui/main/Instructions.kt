package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.*
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment

class Instructions : QuizFragment() {

    companion object {
        fun newInstance() = Instructions()
    }

    val leftEyeListener = object : Listener {
        override fun doTask()
        {
            loadNextScreen.invoke()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.instructions_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        message = activity!!.findViewById(R.id.message)
        viewModel.nextScreen = GameStart.newInstance()

        val messages:Array<TimedMessage> = arrayOf(TimedMessage(getString(R.string.instructionsMsg1), 6),
            TimedMessage(getString(R.string.instructionsMsg2), 5),
            TimedMessage(getString(R.string.instructionsMsg3), 3),
            CustomTimedMessage(getString(R.string.instructionsMsg4),  2, R.font.onetwotree, 58.0f),
            CustomTimedMessage(getString(R.string.instructionsMsg5), 4, R.font.onetwotree, getDefaultFontSize()),
            ActionTimedMessage(getString(R.string.instructionsMsg6), 0, setLeftClosedListener))

        startTimedMessage(message, messages)

    }

    val setLeftClosedListener: ()->Unit = fun(){
        setOnLeftClosedListener(leftEyeListener)
    }

}

package com.gaurdianaq.hauntedai.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.R

class LogicQuestion5 : QuizFragment()
{
    private lateinit var choice1:QuizActionButton
    private lateinit var choice2:QuizActionButton
    private lateinit var choice3:QuizActionButton
    private lateinit var choice4:QuizActionButton

    companion object {
        fun newInstance() = LogicQuestion5()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.logicquestion5_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        choice1 = activity!!.findViewById(R.id.choice1)
        choice1.setUpButton(incorrect, Response.newInstance())
        choice2 = activity!!.findViewById(R.id.choice2)
        choice2.setUpButton(correct, Response.newInstance())
        choice3 = activity!!.findViewById(R.id.choice3)
        choice3.setUpButton(incorrect, Response.newInstance())
        choice4 = activity!!.findViewById(R.id.choice4)
        choice4.setUpButton(incorrect, Response.newInstance())
        viewModel.nextScreen = LogicQuestion6.newInstance()
    }

    val correct: ()->Unit = fun()
    {
        viewModel.responseCorrect = true

    }

    val incorrect: ()->Unit = fun()
    {
        viewModel.responseCorrect = false
    }

}
package com.gaurdianaq.hauntedai

import android.content.Context
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.gaurdianaq.hauntedai.EventHandling.MLKitFaceHandler
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import java.lang.Exception

const val UNLIKELYPERCENTAGE = 0.2f
const val LIKELYPERCENTAGE = 0.8f

class FaceAnalyzer(context: Context) : ImageAnalysis.Analyzer {

    private val context: Context
    private var detecting:Boolean
    private var numBlinks: Int
    private lateinit var handler:MLKitFaceHandler
    private var lastFaces: Array<FirebaseVisionFace>
    var lookingAtScreen: Boolean
        private set
    var leftEyeClosed: Boolean
        private set
    var rightEyeClosed: Boolean
        private set


    init
    {
        this.context = context
        this.detecting = false
        this.numBlinks = 0
        this.leftEyeClosed = false
        this.rightEyeClosed = false
        this.lookingAtScreen = true
        this.lastFaces = arrayOf()
    }

    // High-accuracy landmark detection and face classification
    val realTimeLandClass = FirebaseVisionFaceDetectorOptions.Builder()
        .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
        .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
        .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
        .setMinFaceSize(0.1f) //face needs to be close enough to the screen, will also help prevent faces in the background from being detected
        .build()

    val detector = FirebaseVision.getInstance()
        .getVisionFaceDetector(realTimeLandClass)

    private fun degreesToFirebaseRotation(degrees: Int): Int = when(degrees) {
        0 -> FirebaseVisionImageMetadata.ROTATION_0
        90 -> FirebaseVisionImageMetadata.ROTATION_90
        180 -> FirebaseVisionImageMetadata.ROTATION_180
        270 -> FirebaseVisionImageMetadata.ROTATION_270
        else -> throw Exception("Rotation must be 0, 90, 180, or 270.")
    }

    override fun analyze(imageProxy: ImageProxy?, degrees: Int) {
        val mediaImage = imageProxy?.image
        val imageRotation = degreesToFirebaseRotation(degrees)
        if (mediaImage != null && !detecting) {
            val image = FirebaseVisionImage.fromMediaImage(mediaImage, imageRotation)
            // Pass image to an ML Kit Vision
            // ...
            detecting = true //ensures the detector doesn't get called again until it previouses the previous call
            val result = detector.detectInImage(image)
                .addOnSuccessListener { faces ->
                    // Task completed successfully
                    // [START_EXCLUDE]
                    // [START get_face_info]
                    if (faces.size == 0)
                    {
                        if (lookingAtScreen)
                        {
                            handler.performOnLookAway()
                        }
                        lookingAtScreen = false
                    }
                    else
                    {
                        if (lastFaces.isEmpty())
                        {
                            if (!lookingAtScreen)
                            {
                                handler.performOnLookAtScreen()
                                lookingAtScreen = true
                            }
                        }

                        //left/right eyed probability seem to be inversed when using the front facing camera
                        if (leftEyeClosed && faces[0].rightEyeOpenProbability > LIKELYPERCENTAGE)
                        {
                            handler.performOnLeftOpen()
                            leftEyeClosed = false;
                        }
                        else if (!leftEyeClosed && faces[0].rightEyeOpenProbability < UNLIKELYPERCENTAGE)
                        {
                            handler.performOnLeftClosed()
                            leftEyeClosed = true
                        }

                        //left/right eyed probability seem to be inversed when using the front facing camera
                        if (rightEyeClosed && faces[0].leftEyeOpenProbability > LIKELYPERCENTAGE)
                        {
                            handler.performOnRightOpen()
                            rightEyeClosed = false;
                        }
                        else if (!rightEyeClosed && faces[0].leftEyeOpenProbability < UNLIKELYPERCENTAGE)
                        {
                            handler.performOnRightClosed()
                            rightEyeClosed = true
                        }


                        if (faces[0].smilingProbability > LIKELYPERCENTAGE)
                        {
                            handler.performOnSmile()
                        }

                    }

                    //if (faces[0].)
                    lastFaces = faces.toTypedArray() //assign the last processed faces so we can access them from outside the analyzer

                    detecting = false
                    // [END get_face_info]
                    // [END_EXCLUDE]
                }
                .addOnFailureListener { e ->
                    // Task failed with an exception
                    // ...
                }
        }
    }



    fun registerHandler(handler: MLKitFaceHandler)
    {
        this.handler = handler
    }

    fun leftEyeClosed() : Boolean
    {
        return false
    }
}
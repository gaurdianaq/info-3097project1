package com.gaurdianaq.hauntedai.EventHandling

interface MLKitFaceHandler
{
    fun performOnLeftOpen()
    fun performOnLeftClosed()
    fun performOnRightOpen()
    fun performOnRightClosed()
    fun performOnLookAway()
    fun performOnLookAtScreen()
    fun performOnSmile()
}
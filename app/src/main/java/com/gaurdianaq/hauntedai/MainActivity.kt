package com.gaurdianaq.hauntedai

import android.Manifest
import android.content.pm.PackageManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Size
import android.view.WindowManager
import androidx.camera.core.CameraX
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysisConfig
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProviders
import com.gaurdianaq.hauntedai.CustomUI.QuizActionButton
import com.gaurdianaq.hauntedai.ui.main.Instructions
import java.util.concurrent.Executors

private const val REQUEST_CODE_PERMISSIONS = 10

class MainActivity : AppCompatActivity(), LifecycleOwner {
    var mediaPlayer: MediaPlayer? = null
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private val config = ImageAnalysisConfig.Builder()
        .setTargetResolution(Size(720, 1280))
        .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        .setLensFacing(CameraX.LensFacing.FRONT)
        .build()

    private val imageAnalysis = ImageAnalysis(config)
    private val executor = Executors.newSingleThreadExecutor()
    private lateinit var viewModel: MainViewModel
    private lateinit var analyzer: FaceAnalyzer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mediaPlayer = MediaPlayer.create(this, R.raw.ambiance1)
        mediaPlayer?.isLooping = true
        initTimedMessagesContext(this)
        if (!allPermissionsGranted())
        {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
        else
        {
            analyzer = FaceAnalyzer(this)
            imageAnalysis.setAnalyzer(executor, analyzer)
            viewModel.setFaceAnalyzer(analyzer)

            CameraX.bindToLifecycle(this as LifecycleOwner, imageAnalysis)
        }
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, Instructions.newInstance())
                .commitNow()
        }

        QuizActionButton.init(this)

    }

    override fun onResume() {
        super.onResume()
        mediaPlayer?.start()
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer?.pause()
    }

    override fun onStop() {
        super.onStop()
        mediaPlayer?.stop()
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

}

package com.gaurdianaq.hauntedai

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class About : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }

    fun start(view: View)
    {
        val startGame = Intent(this, MainActivity::class.java)
        startActivity(startGame)
    }
}

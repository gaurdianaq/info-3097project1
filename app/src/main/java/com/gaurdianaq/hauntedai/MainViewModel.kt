package com.gaurdianaq.hauntedai

import androidx.camera.core.Preview
import androidx.lifecycle.ViewModel
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment
import com.gaurdianaq.hauntedai.EventHandling.MLKitFaceHandler

class MainViewModel : ViewModel() {
    private lateinit var faceAnalyzer: FaceAnalyzer
    lateinit var nextScreen:QuizFragment
    var responseCorrect:Boolean
    var lookedAround:Boolean
    var lied:Boolean
    var alone:Boolean
    var alonecertain:Boolean

    init {
        responseCorrect = false
        lookedAround = false
        lied = false
        alone = true
        alonecertain = false
    }

    fun setFaceAnalyzer(analyzer:FaceAnalyzer)
    {
        faceAnalyzer = analyzer
    }

    fun registerHandler(handler: MLKitFaceHandler)
    {
        faceAnalyzer.registerHandler(handler)
    }

    fun lookingAtScreen(): Boolean
    {
        return faceAnalyzer.lookingAtScreen
    }
}

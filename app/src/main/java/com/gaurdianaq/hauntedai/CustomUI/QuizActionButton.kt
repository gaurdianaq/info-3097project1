package com.gaurdianaq.hauntedai.CustomUI

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gaurdianaq.hauntedai.R

class QuizActionButton(context: Context, attr:AttributeSet) : Button(context, attr)
{
    private lateinit var action: () -> Unit
    private lateinit var launchFragment: Fragment
    companion object
    {
        private lateinit var _activity: AppCompatActivity
        fun init(activity: AppCompatActivity) //must be called before any instances of button
        {
            _activity = activity
        }
    }

    init {
        if (!isClickable)
        {
            isClickable = true
        }
        setOnClickListener(fun(view: View)
        {

            action.invoke()//perform whatever action was passed and required of it

            _activity.supportFragmentManager.beginTransaction().replace(R.id.container, launchFragment).commit()
        })
    }

    fun setUpButton(launchFragment: Fragment)
    {
        this.launchFragment = launchFragment
        this.action = fun(){} //set it to a dummy/empty function if nothing is provided
    }

    //call this when button is created
    fun setUpButton(action:()->Unit, launchFragment: Fragment)
    {
        this.action = action
        this.launchFragment = launchFragment
    }
}
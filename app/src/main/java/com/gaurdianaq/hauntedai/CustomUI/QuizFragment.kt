package com.gaurdianaq.hauntedai.CustomUI

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.gaurdianaq.hauntedai.EventHandling.*
import com.gaurdianaq.hauntedai.MainViewModel
import com.gaurdianaq.hauntedai.R

//Overrided fragment for things that will be common across all quiz fragment instances (such as the view model, saves me a bit of boilerplate and can add new things to it without needing to add them to every fragment
abstract class QuizFragment : Fragment(), MLKitFaceHandler
{
    interface Listener
    {
        fun doTask()
    }

    private var mOnLeftOpenListener: Listener? = null
    private var mOnLeftClosedListener: Listener? = null
    private var mOnRightOpenListener: Listener? = null
    private var mOnRightClosedListener: Listener? = null
    private var mOnLookAwayListener: Listener? = null
    private var mOnLookAtScreenListener: Listener? = null
    private var mOnSmileListener: Listener? = null


    protected lateinit var viewModel: MainViewModel
    protected lateinit var message: TextView //every quiz fragment is likely to have a message, and if it doesn't then I'll just set it to blank


    override fun performOnLeftOpen() {
        if (mOnLeftOpenListener != null)
        {
            mOnLeftOpenListener?.doTask()
        }
    }

    override fun performOnLeftClosed() {
        if (mOnLeftClosedListener != null)
        {
            mOnLeftClosedListener?.doTask()
        }
    }

    override fun performOnRightOpen() {
        if (mOnRightOpenListener != null)
        {
            mOnRightOpenListener?.doTask()
        }
    }

    override fun performOnRightClosed() {
        if (mOnRightClosedListener != null)
        {
            mOnRightClosedListener?.doTask()
        }
    }

    override fun performOnLookAtScreen() {
        if (mOnLookAtScreenListener != null)
        {
            mOnLookAtScreenListener?.doTask()
        }
    }

    override fun performOnLookAway() {
        if (mOnLookAwayListener != null)
        {
            mOnLookAwayListener?.doTask()
        }
    }

    override fun performOnSmile() {
        if (mOnSmileListener != null)
        {
            mOnSmileListener?.doTask()
        }
    }

    fun setOnLeftOpenListener(listener: Listener)
    {
        mOnLeftOpenListener = listener
    }

    fun setOnLeftClosedListener(listener: Listener) {
        mOnLeftClosedListener = listener
    }

    fun setOnRightOpenListener(listener: Listener)
    {
        mOnRightOpenListener = listener
    }

    fun setOnRightClosedListener(listener: Listener)
    {
        mOnRightClosedListener = listener
    }

    fun setOnLookAwayListener(listener: Listener)
    {
        mOnLookAwayListener = listener
    }

    fun setOnLookAtScreenListener(listener: Listener)
    {
        mOnLookAtScreenListener = listener
    }

    fun setOnSmileListener(listener: Listener)
    {
        mOnSmileListener = listener
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let { viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java) }
        viewModel.registerHandler(this) //should ensure the handler is always pointing to the correct fragment
        message = activity!!.findViewById(R.id.message)//guaranteeing that all layouts for every question have a textview called message
    }

    fun getDefaultFontSize() : Float
    {
        return resources.getDimension(R.dimen.fontSize) / 2
    }

    //make sure you set the nextScreen before calling this
    val loadNextScreen: ()->Unit = fun()
    {
        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container, viewModel.nextScreen)?.commit()
    }


}
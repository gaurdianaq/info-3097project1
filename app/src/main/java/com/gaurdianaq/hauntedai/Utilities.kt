package com.gaurdianaq.hauntedai

import android.content.Context
import android.os.CountDownTimer
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.gaurdianaq.hauntedai.CustomUI.QuizFragment

open class TimedMessage (val message:String, val time:Long)

class CustomTimedMessage (message:String, time: Long, val fontID:Int, val fontSize:Float) : TimedMessage(message, time)
class ActionTimedMessage (message:String, time: Long, val action: () -> Unit) : TimedMessage(message, time)

private class TimedMessages(textView: TextView, messages:Array<TimedMessage>)
{
    companion object {
        lateinit var context: Context
    }
    private inner class MessageTimer(time:Long, interval:Long) : CountDownTimer(time, interval) {

        override fun onFinish() {
            if (messages[count+1] is CustomTimedMessage)
            {
                val cMessage = messages[count+1] as CustomTimedMessage
                textView.typeface = ResourcesCompat.getFont(context, cMessage.fontID)
                textView.textSize = cMessage.fontSize
            }
            if (messages[count+1] is ActionTimedMessage)
            {
                val lMessage = messages[count+1] as ActionTimedMessage
                lMessage.action.invoke()
            }
            textView.text = messages[count+1].message
            ++count
            if (count != messages.size-1)
            {
                nextMessage()
            }
        }

        override fun onTick(millisUntilFinished: Long) {
        }
    }

    private var timer:MessageTimer
    private var count:Int
    private val textView:TextView
    private val INTERVAL:Long = 1000
    private val messages:Array<TimedMessage>

    init {
        this.count = 0
        this.messages = messages
        this.textView = textView
        this.timer = MessageTimer(messages[0].time*1000, INTERVAL)
        this.timer.start()
    }

    private fun nextMessage()
    {
        this.timer = MessageTimer(messages[count].time*1000, INTERVAL)
        this.timer.start()
    }
}

fun initTimedMessagesContext(context: Context)
{
    TimedMessages.context = context
}

fun startTimedMessage(textView: TextView, messages: Array<TimedMessage>)
{
    textView.text = messages[0].message
    TimedMessages(textView, messages)
}